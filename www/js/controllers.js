angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Grammar, $sessionStorage, $ionicPopover, Categories, Data) {
  var template = '<ion-popover-view><ion-header-bar> <h1 class="title">My Popover Title</h1> </ion-header-bar> <ion-content> Hello! </ion-content></ion-popover-view>';

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });

  // .fromTemplateUrl() method
  $ionicPopover.fromTemplateUrl('my-popover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });


  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  Categories.getData()
    .success(function(data, status){
      console.log(status);
      console.log(data.categories);
      $scope.categories=data.categories;
    })
    .error(function(data, status){
      console.log(status);
    })
  $scope.data = [];
  $scope.loading= true;
  if ($sessionStorage.length===undefined){
    Grammar.getData()
    .success(function(data, status){
      console.log(status);
      $sessionStorage.status = status;
      $scope.data = data.posts;
      $scope.loading = false;
      $sessionStorage.data = data.posts;
    })
    .error(function(data, status){
      console.log(status);
    })
  }
  else {
    $scope.loading = false;
    $scope.data = $sessionStorage.data;
  }
  $scope.loadNewData = function(url){
    console.log(url)
    console.log('works!');
    $scope.data = [];
    $scope.loading = true;
  Data.get(url)
    .success(function(data, status){
      $scope.data=data.posts;
        $scope.loading = false;
      $sessionStorage.data = $scope.data;
    })
    .error(function(data, status){
      console.log(status);
    })
  }
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, Categories) {
  $scope.settings = {
    enableFriends: true
  };

})
.controller('IdCtrl', function($scope,  $stateParams, Grammar, $sessionStorage) {
  console.log($stateParams.id);
  $scope.loading=true;
    $scope.data = $sessionStorage.data;
    for (var i=0; i < $sessionStorage.data.length; i++){
      if ($scope.data[i].ID == $stateParams.id){
        $scope.loading =false;
        $scope.post = $scope.data[i];
      }
    }        //console.log($scope.data[0].ID);
});



